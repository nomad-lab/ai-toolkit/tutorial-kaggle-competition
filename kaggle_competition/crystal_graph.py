# Copyright 2018 Tony Y. https://www.kaggle.com/tonyyy All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# =========================================================================

import numpy as np
import pandas as pd
import networkx as nx
import matplotlib.pyplot as plt
import warnings
warnings.filterwarnings("ignore")

def length(v):
    return np.linalg.norm(v)

def unit_vector(vector):
    return vector / length(vector)

def angle_between(v1, v2):
    v1_u = unit_vector(v1)
    v2_u = unit_vector(v2)
    return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))

def angle_deg_between(v1, v2):
    return np.degrees(angle_between(v1, v2))

def get_xyz_data(filename):
    pos_data = []
    sym_data = []
    lat_data = []
    with open(filename) as f:
        for line in f.readlines():
            x = line.split()
            if x[0] == 'atom':
                pos_data.append(np.array(x[1:4], dtype=np.float))
                sym_data.append(x[4])
            elif x[0] == 'lattice_vector':
                lat_data.append(np.array(x[1:4], dtype=np.float))
    return np.array(pos_data), sym_data, np.array(lat_data)

def get_shortest_distances(reduced_coords, amat, R_max):
    lmn = []
    for l in range(-1, 2):
        for m in range(-1, 2):
            for n in range(-1, 2):
                lmn.append(np.array([l, m, n]))
    lmn = np.array(lmn)

    rij = reduced_coords[:,np.newaxis] - reduced_coords[np.newaxis,:]
    rij = rij[:, :, np.newaxis, :] + lmn[np.newaxis, np.newaxis]
    Rij = np.matmul(rij, np.transpose(amat))
    rij = np.linalg.norm(Rij, axis=3)
    ijk_crdn = np.where((rij < R_max[:,:,np.newaxis]) & (rij > 1e-8))
    crdn = []
    for n in range(len(reduced_coords)):
        crdn.append([])
    for n in range(len(ijk_crdn[0])):
        i, j, k = ijk_crdn[0][n], ijk_crdn[1][n], ijk_crdn[2][n]
        crdn[j].append([i, rij[i,j,k], Rij[i,j,k]])
    ij_min = np.argmin(rij, axis=2)
    rij = [ [rij[i, j, ij_min[i,j]] for j in range(len(ij_min))] for i in range(len(ij_min)) ]
    rij = np.array(rij)
    Rij = [ [Rij[i, j, ij_min[i,j]] for j in range(len(ij_min))] for i in range(len(ij_min)) ]
    Rij = np.array(Rij)

    return rij, Rij, crdn

#
# Database of Ionic Radii
# http://abulafia.mt.ic.ac.uk/shannon/ptable.php
#
# Coordination IV
R_O = 1.35
#
# Coordination VI
R_Al = 0.535
R_Ga = 0.62
R_In = 0.8
#
R_ionic = { "O" : R_O, "Al" : R_Al, "Ga" : R_Ga, "In" : R_In }

def get_Rmax(symbols):
    natom = len(symbols)
    R_max = [[ R_ionic[symbols[i]] + R_ionic[symbols[j]] if (symbols[i] == "O" and symbols[j] != "O") or (symbols[i] != "O" and symbols[j] == "O") else 0.1 for j in range(natom) ] for i in range(natom)]

    return np.array(R_max)

def get_factor(spacegroup, gamma):
    if spacegroup == 12:
        return 1.4
    elif spacegroup == 33:
        return 1.4
    elif spacegroup == 167:
        return 1.5
    elif spacegroup == 194:
        return 1.3
    elif spacegroup == 206:
        return 1.5
    elif spacegroup == 227:
        if gamma < 60:
            return 1.4
        else:
            return 1.5
    else:
        raise NameError('get_factor does not support the spacegroup: {}'.format(spacegroup))

def get_crystal_graph(crdn, symbols):
    G = nx.MultiDiGraph()
    for i in range(len(symbols)):
        G.add_node(i, symbol=symbols[i])
    for i in range(len(crdn)):
        for c in crdn[i]:
            j, d, R = tuple(c)
            G.add_edge(i, j, d=d, R=R)

    return G

def print_crytal_graph(G, filename="crystal_graph.pdf"):
    g = nx.nx_pydot.to_pydot(G)
    for node in g.get_nodes():
        symbol = node.get_attributes()["symbol"]
        node.set_label("{}_{}".format(symbol, node.get_name()))
        if symbol == "O":
            node.set_color("red")
        else:
            node.set_color("green")
    g.write_pdf('crystal_graph.pdf', prog='neato')

def get_features(fn, factor):
    crystal_xyz, crystal_sym, crystal_lat = get_xyz_data(fn)
    A = np.transpose(crystal_lat)
    B = np.linalg.inv(A)
    crystal_red = np.matmul(crystal_xyz, np.transpose(B))
    R_max = get_Rmax(crystal_sym) * factor
    crystal_dist, crystal_Rij, crystal_crdn = get_shortest_distances(crystal_red, A, R_max)

    G = get_crystal_graph(crystal_crdn, crystal_sym)

    print_crytal_graph(G)

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Generate the crystal graph.')
    parser.add_argument('file', metavar='FILE_PATH', type=str,
                        help='the path to a geometry file in the XYZ format')
    parser.add_argument('factor', metavar='CG_FACTOR', type=float, nargs='?',
                        help='the factor of the connection threshold (1.5)', default=1.5)
    args = parser.parse_args()

    get_features(args.file, args.factor)
